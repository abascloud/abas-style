# ABAS Style #

This is a style Web Component it contains all essential Styling for Web Components made by ABAS.

## How to use ##

Include in your index.html

```HTML   
<head>
    <link rel="import" href="../abas-style/abas-style.html">
    <style is="custom-style" include="abas-style"></style>
</head>
```

## CSS Variables ##

| Variable                  | Value   |
| ------------------------- | ------- |
| `--hero-green`            | #1bbaa5 |
| `--hero-green-hover`      | #8dddd2 |
| `--hero-grey`             | #2a3539 |
| `--hero-grey-hover`       | #a0a4a7 |
| `--white`                 | #ffffff |
| `--light-grey`            | #cbccce |
| `--very-light-grey`       | #f4f4f4 |
| `--mid-grey`              | #525b60 |
| `--alert-red`             | #cd2735 |
| `--alert-orange`          | #e68213 |
| `--alert-yellow`          | #ebc80a |
| `--alert-green`           | #84c32c |
| `--alert-blue`            | #1a5dad |
| `--accent-red`            | #961937 |
| `--accent-mustard`        | #ffb60f |
| `--accent-green`          | #1fa870 |
| `--accent-teal`           | #0b7b7c |
| `--accent-blue`           | #1c5e6e |
| `--abas-accent-red`       | #e30047 |
| `--abas-accent-red-hover` | #da7093 |
| `--abas-accent-yellow`    | #fce122 |
| `--abas-accent-green`     | #96d700 |

## Fonts ##

1. abasPermMarker ![abasPermMarker.png](https://bitbucket.org/abascloud/abas-style/raw/master/img/abasPermMarker.PNG "abasPermMarker.png")
